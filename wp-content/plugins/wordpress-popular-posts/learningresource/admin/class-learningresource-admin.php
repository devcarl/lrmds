<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.junefajardo.com
 * @since      1.0.0
 *
 * @package    Learningresource
 * @subpackage Learningresource/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Learningresource
 * @subpackage Learningresource/admin
 * @author     juniel <junefajardo94@gmail.com>
 */
class Learningresource_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Learningresource_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Learningresource_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/learningresource-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Learningresource_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Learningresource_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/learningresource-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function learningresource_register_post() {

		$labels = array(
			'name'              => _x( 'Learning Resource', 'learningresource' ),
			'singular_name'     => _x( 'Learning Resource', 'learningresource' ),
			'add_new_item'      => __( 'Add New ' . 'Learning Resources' ),
			'search_items'      => __( 'Search '. 'Learning Resources' ),
			'all_items'         => __( 'Learning Resources' ),
			'parent_item'       => __( 'Parent Item ' . 'Learning Resources' ),
			'parent_item_colon' => __( 'Parent Items colon' . 'Learning Resources' ),
			'edit_item'         => __( 'Edit ' . 'Learning Resource' ),
			'update_item'       => __( 'Update ' . 'Learning Resource' ),
			'new_item_name'     => __( 'New ' . 'Learning Resource' ),
			'menu_name'         => __( 'Learning Resources' )
		);


		$supports = array(
			'title',
			'comments',
			'editor',
			'thumbnail',        
			'author',
			'custom-fields',
			'page-attributes',
		);

		$details = array(
			'slug'              => 'learning_resource', 
			'labels'            => $labels, 
			'description'       => 'Everything you want to know about a Learning Resources',
			'public'            => true,
			'supports'          => $supports,
			'has_archive'       => true
		);

		register_post_type( 'learning_resource', $details );

		$labels2 = array(
			'name'              => _x( 'Media Resource', 'learningresource' ),
			'singular_name'     => _x( 'Media Resource', 'learningresource' ),
			'add_new_item'      => __( 'Add New ' . 'Media Resources' ),
			'search_items'      => __( 'Search '. 'Media Resources' ),
			'all_items'         => __( 'Media Resources' ),
			'parent_item'       => __( 'Parent Item ' . 'Media Resources' ),
			'parent_item_colon' => __( 'Parent Items colon' . 'Media Resources' ),
			'edit_item'         => __( 'Edit ' . 'Media Resource' ),
			'update_item'       => __( 'Update ' . 'Media Resource' ),
			'new_item_name'     => __( 'New ' . 'Media Resource' ),
			'menu_name'         => __( 'Media Resources' )
		);


		$supports2 = array(
			'title',
			'comments',
			'editor',
			'thumbnail',        
			'author',
			'custom-fields',
			'page-attributes',
		);

		$details2 = array(
			'slug'              => 'media_resource',    
			'labels'            => $labels2,    
			'description'       => 'Everything you want to know about a Media Resources',
			'public'            => true,
			'supports'          => $supports2,
			'has_archive'       => true,
			//'show_in_menu'      => 'edit.php?post_type=learning_resource'
		);

		register_post_type( 'media_resource', $details2 );


		$labels3 = array(
			'name'              => _x( 'Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Categories' ),
			'all_items'         => __( 'All Categories' ),
			'parent_item'       => __( 'Parent Category' ),
			'parent_item_colon' => __( 'Parent Category:' ),
			'edit_item'         => __( 'Edit Category' ),
			'update_item'       => __( 'Update Category' ),
			'add_new_item'      => __( 'Add New Category' ),
			'new_item_name'     => __( 'New Category Name' ),
			'menu_name'         => __( 'Categories' ),
		);

		$args3 = array(
			'hierarchical'      => true,
			'labels'            => $labels3,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'categories' ),
		);

		 register_taxonomy( 'learning_categories', array( 'media_resource' ), $args3 );

		$labels4 = array(
			'name'              => _x( 'Levels', 'taxonomy general name' ),
			'singular_name'     => _x( 'Level', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Levels' ),
			'all_items'         => __( 'All Levels' ),
			'parent_item'       => __( 'Parent Level' ),
			'parent_item_colon' => __( 'Parent Level:' ),
			'edit_item'         => __( 'Edit Level' ),
			'update_item'       => __( 'Update Level' ),
			'add_new_item'      => __( 'Add New Level' ),
			'new_item_name'     => __( 'New Level Name' ),
			'menu_name'         => __( 'Levels' ),
		);

		$args4 = array(
			'hierarchical'      => true,
			'labels'            => $labels4,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'categories' ),
		);

		register_taxonomy( 'learning_level', array( 'learning_resource' ), $args4 );


		$labels5 = array(
			'name'              => _x( 'Learning Areas', 'taxonomy general name' ),
			'singular_name'     => _x( 'Learning Area', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Learning Areas' ),
			'all_items'         => __( 'All Learning Areas' ),
			'parent_item'       => __( 'Parent Learning Area' ),
			'parent_item_colon' => __( 'Parent Learning Area:' ),
			'edit_item'         => __( 'Edit Learning Area' ),
			'update_item'       => __( 'Update Learning Area' ),
			'add_new_item'      => __( 'Add New Learning Area' ),
			'new_item_name'     => __( 'New Learning Area Name' ),
			'menu_name'         => __( 'Learning Areas' ),
		);

		$args5 = array(
			'hierarchical'      => true,
			'labels'            => $labels5,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'categories' ),
		);

		 register_taxonomy( 'learning_area', array( 'learning_resource' ), $args5 );


	}

	public function learningresource_settings_init() {

		 register_setting( 'learningresource', 'learning_options' );
		 
		 add_settings_section(
			'learningresource_section_developers',
			 __( 'Resource fields', 'learningresource' ),
			array( $this, 'learningresource_section_developers_cb' ),
			'learningresource-section'
		 );

		 $option_val = get_option( 'learning_options' );
		 
		 add_settings_field(
			'learningresource_grade_level',
			 __( 'Grade Level', 'learningresource' ),
			array( $this, 'learningresource_field_grave_level' ),
			'learningresource-section',
			'learningresource_section_developers',
			array(
				'label_for' => 'learningresource_grade_level',
				'class' => 'learningresource_row',
				'value' => ( isset( $option_val['grade_level'] ) ) ? $option_val['grade_level'] : ''
			)
		);

		add_settings_field(
			'learningresource_learning_area',
			 __( 'Learning Area', 'learningresource' ),
			array( $this, 'learningresource_field_learning_area' ),
			'learningresource-section',
			'learningresource_section_developers',
			array(
				'label_for' => 'learningresource_learning_area',
				'class' => 'learningresource_row',
				'value' => ( isset( $option_val['learning_area'] ) ) ? $option_val['learning_area'] : ''
			)
		);
	}

	public function learningresource_field_grave_level( $args ) {
		?>
		<textarea cols="100" rows="10" name="learning_options[grade_level]" id="<?php echo esc_attr( $args['label_for'] ); ?>"><?php echo esc_attr( $args['value'] ); ?></textarea>
		<?php
		
	}

	public function learningresource_field_learning_area( $args ) {
		?>
		<textarea cols="100" rows="10" name="learning_options[learning_area]" id="<?php echo esc_attr( $args['label_for'] ); ?>"><?php echo esc_attr( $args['value'] ); ?></textarea>
		<?php
	}

	public function learningresource_section_developers_cb( $args ) {}


	public function learningresource_options_page() {

		add_menu_page(
			'Learning Resource Settings',
			'Learning Resource Options',
			'manage_options',
			'learningresource',
			array( $this, 'learningresource_options_page_html' )
		);
	}

	public function learningresource_options_page_html() {

		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		if ( isset( $_GET['settings-updated'] ) ) {
			add_settings_error( 'learningresorce_messages', 'learningresorce_messages', __( 'Settings Saved', 'learningresource' ), 'updated' );
		}

		settings_errors( 'learningresorce_messages' );
		?>
			<div class="wrap">
				<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
				<form action="options.php" method="post">
					<?php
					settings_fields( 'learningresource' );
					do_settings_sections( 'learningresource-section' );
					submit_button( 'Save Settings' );
					?>
				</form>
			</div>
		<?php

	}


}
