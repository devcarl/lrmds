<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.junefajardo.com
 * @since      1.0.0
 *
 * @package    Learningresource
 * @subpackage Learningresource/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Learningresource
 * @subpackage Learningresource/public
 * @author     juniel <junefajardo94@gmail.com>
 */
class Learningresource_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Learningresource_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Learningresource_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( 'datatables-style', plugin_dir_url( __FILE__ ) . 'css/datatables.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootsrap-style', plugin_dir_url( __FILE__ ) . 'css/bootstrap.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/learningresource-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Learningresource_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Learningresource_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'datatables-script', plugin_dir_url( __FILE__ ) . 'js/datatables.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/learningresource-public.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'bootsrap-script', plugin_dir_url( __FILE__ ) . 'js/bootstrap.js', array( 'jquery' ), $this->version, false );

		wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}
	public function learningresource_download_file(){

		$filedata = $_POST['data'];

		$update = update_post_meta( $filedata['ID'], 'lrmds-download-count', $filedata['count'] );

		wp_send_json_success( $update );

	}

	public function learning_page_template( $template ) {

		global $wp_query;

		if ( $wp_query->query['post_type'] == "learning_resource" ) {
			$template =  dirname( __FILE__ ) . '/template/single-resource.php';
		}

		if ( $wp_query->query['post_type'] == "media_resource" ) {
			$template =  dirname( __FILE__ ) . '/template/single-media.php';
		}

		return $template;
	}

	public function learningresource_upload() {

		

		if( ! empty( $_FILES['file'] ) ) {

			 wp_send_json_success( $_FILES['file'] );
		}


		if( isset( $_POST['datas'] ) ) {

			$post =  $_POST['datas']['data'];
			$file =  $_POST['datas']['file'];
		
			$postArr = array(
				'post_title' => ( isset( $post['title'] ) ) ? $post['title'] : '',
				'post_content' => ( isset( $post['description'] ) ) ? $post['description'] : '',
				'post_status' => 'publish',
				'post_type' => 'learning_resource',
			);

			unset($post['title']);
			unset($post['description']);

			$postID = wp_insert_post( $postArr );

			// insert new learning resource

			if( $postID ) {

				foreach( $post as $key => $val ) {

					if( 'lrmds-grade-level' == $key ) {
						wp_set_object_terms( $postID, explode( ',', $val ), true );
					} else if( 'lrmds-learning-area' == $key ) {
						wp_set_object_terms( $postID, explode( ',', $val ), true );
					} else {
						update_post_meta( $postID, $key, $val );
					}
				}

				if( ! empty( $file ) && isset( $file['data']['name'] ) ) {

					$filename =  $file['data']['name'];

					$filePath = wp_upload_bits( $filename, null, file_get_contents(  $file['data']['tmp_name'] ) );

					$ftitle = substr( $filename, 0, strpos( $filename, '.' ) );
					$ftype = substr( $filename, strpos( $filename, '.' ), -1 );

					update_post_meta( $postID, 'lrmds-file-name', $ftitle );
					update_post_meta( $postID, 'lrmds-file-type', $file['data']['type'] );
					update_post_meta( $postID, 'lrmds-file-extension', explode( '/', $file['data']['type'] )[1] );
					update_post_meta( $postID, 'lrmds-file-size', $file['data']['size'] );
					update_post_meta( $postID, 'lrmds-resource-url', $filePath['url'] );
					update_post_meta( $postID, 'lrmds-file-path', str_replace('\\', '/', $filePath['file'] ) );
				}
			}

			wp_send_json_success( $postArr ) ;
		}
	}

	public function html_content( $array, $parts ) {

		ob_start();
		extract( $array );
		include 'template/' . $parts;
		$page =  ob_get_contents();
		return ob_get_clean();
	}

	public function term_object_to_array( $object, $find ) {

		for( $x = 0,$newArr = []; $x < count( $object ); $x++ ) {
			if( $find == 'term_id' ) {
				$newArr[] = $object[$x]->term_id;
			}
			if( $find == 'name' ) {
				$newArr[] = $object[$x]->name;
			}
		}

		return $newArr;
	}


	public function learningresource_upload_shortcode() {

		global $pagename;

		// check if it's a login user
		if( ! is_user_logged_in() ) {
			return;
		}

		// check if the user role is valid
		$user = wp_get_current_user();

		if( ! array_intersect( array('editor','administrator','author'), $user->roles ) ) {
			return;
		}

		$option_val = get_option( 'learning_options' );

		$learningArea =	( isset( $option_val['learning_area'] ) ) ? preg_split( '/\r\n|\r|\n/', $option_val['learning_area'] ) : [];

		$gradeLevel =	( isset( $option_val['grade_level'] ) ) ? preg_split( '/\r\n|\r|\n/', $option_val['grade_level'] ) : [];


		$html = $this->html_content(
			array(
				'learningArea' => $learningArea,
				'gradeLevel' => $gradeLevel
			), 'resource-upload.php'
		);


		return $html;
	}


	public function learningresource_education_sidebar( $atts ) {

		global $pagename;

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		if( $_GET['level'] == 'basic literacy' ) {
			return;
		}

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'meta_key' => 'lrmds-education-type',
			'meta_value' => $var['education-type'],
		);

		$posts = get_posts( $args );

		$grouplevel = [];

		foreach( $posts as $post ) {

			$grade = get_the_terms( $post->ID, 'learning_level' );

			if( ! empty( $grade ) ) {

				for( $x = 0; $x < count( $grade ); $x++ ) {
					if( strtolower( $grade[$x]->slug ) != 'basic-literacy' ) {
						$grouplevel[$grade[$x]->name][] = $post->ID;
					}
				}
			}
		}

		$keysArr = array_keys( $grouplevel );

		usort( $keysArr, function( $x, $y ) {

			$x_s = filter_var( $x, FILTER_SANITIZE_NUMBER_INT );
			$y_s = filter_var( $y, FILTER_SANITIZE_NUMBER_INT );

			if( $x_s < $y_s ) {
				return  -1;
			} else {
				return 1;
			}

		} );

		for( $y = 0, $newArr = []; $y < count( $keysArr ); $y++ ) {
			$newArr[$keysArr[$y]] = $grouplevel[$keysArr[$y]];
		}

		$html = $this->html_content( array( 'grouplevel' => $newArr ), 'resource-sidebar.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');	

	}

	public function learningresource_page_render( $atts ) {


		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		if ( ! isset( $_GET['level'] ) && $var['education-type'] == 'K to 12' ) {
			return do_shortcode('[et_pb_section][/et_pb_section]');
		}

		$level = ( isset( $_GET['level'] ) ) ? $_GET['level'] : 'basic literacy';

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'lrmds-education-type',
					'value' => $var['education-type']
				),
			),
			'tax_query' => array(
				array(
					'taxonomy' => 'learning_level',
					'field' => 'name',
					'terms' => urldecode( $level ),
				)
			)
		);

		$posts = get_posts( $args );

		foreach( $posts as $post ) {

			$area = get_the_terms( $post->ID, 'learning_area' );
			
			if( ! empty( $area ) ) {

				for( $x = 0, $y = 1; $x < count( $area ); $x++ ) {

					$content = get_post_meta( $post->ID, 'lrmds-content-topic', true );

					if( ! empty( $content ) ) {

						$newArray = array(
							'ID' => $post->ID,
							'title' => $post->post_title
						);

						$learningarea['total'] += $y;
						$learningarea['datas'][$area[$x]->slug]['ID'] = $area[$x]->term_id;
						$learningarea['datas'][$area[$x]->slug]['rows'] += $y;
						$learningarea['datas'][$area[$x]->slug]['content-topics'][$content]['size'] += $y;
						$learningarea['datas'][$area[$x]->slug]['content-topics'][$content]['data'][] = $newArray;
					}
				}
			}
		}

		$level = get_the_terms( get_the_ID(), 'learning_level' );

		$html = $this->html_content(
			array(
				'learningarea' => $learningarea,
				'type' => $var['education-type'],
				'level' => $level
			), 'resource-list.php'
		);

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');	
	}

	public function learningresource_most_popular( $atts ) {

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'meta_key' => 'lrmds-download-count',
			'orderby' => 'meta_value_num',
			'order' => 'DESC',
			'meta_query' => array(
				array(
					'key' => 'lrmds-education-type',
					'value' => $var['education-type']
				)
			)
		);

		$posts = get_posts( $args );

		$html = $this->html_content( array( 'posts' => $posts ), 'most-popular.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');

	}
	public function learningresource_related_resource( $atts ) {

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		$levelArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_level' ), 'term_id' );
		$areaArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_area' ), 'term_id' );

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'exclude'	=> array( get_the_ID() ),
			'meta_query' => array(
				array(
					'key' => 'lrmds-education-type',
					'value' => $var['education-type']
				),
			)
		);

		if( ! empty( $levelArr ) ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'learning_level',
				'field'    => 'term_id',
				'terms'    => $levelArr
			);
		}

		if( ! empty( $areaArr ) ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'learning_area',
				'field'    => 'term_id',
				'terms'    => $areaArr
			);
		}

		$posts = get_posts( $args );

		$html = $this->html_content( array( 'posts' => $posts ), 'related-resource.php' );

		return $html;

	} 

	public function learningresource_topic_list( $atts ) {

		$option_val = get_option( 'learning_options' );

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		$type = ( isset( $_GET['type'] ) ) ?  $_GET['type'] : $var['education-type'];
		$level = ( $var['education-type'] != 'K to 12') ? 'basic literacy' : $_GET['level'];
		$topic = base64_decode( $_GET['topic'] );
		$section = ( isset( $_POST['filter'] ) ) ? $_POST['filter'] : 'all';

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'lrmds-education-type',
					'value' => $type
				)
			)
		);

		if( isset( $_GET['topic'] ) ) {
			$args['meta_query'][] = array(
				'key' => 'lrmds-content-topic',
				'value' => $topic,
				'compare' => '=',
			);
		}

		if( $section != 'all' ) {
			$args['meta_query'][] = array(
				'key' => 'lrmds-education-section',
				'value' => $_POST['filter'],
				'compare' => '=',
			);
		}

		if( isset( $_GET['level'] ) ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'learning_level',
					'field' => 'name',
					'terms' => $level
				)
			);
		}

		$posts = get_posts( $args );

		$html = $this->html_content( array( 
			'posts' => $posts, 
			'level' => $level, 
			'type' => $type,
			'topic' => $topic,
			'section' => $section
		), 'resource-table.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');

	}

	public function learningresource_details( $atts ) {

		$levelArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_level' ), 'name' );
		$areaArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_area' ), 'name' );

		$html = $this->html_content( 
			array( 
				'level' => implode( '|', $levelArr ),
				'area' =>  implode( '|', $areaArr )
			), 
			'resource-detail.php' 
		);

		return $html;
	}

	public function learningresource_media( $atts ) {

		$html = $this->html_content( [], 'resource-media.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}

	public function learningresource_media_list( $atts ) {

		$html = $this->html_content( array( 'categories' => get_terms('learning_categories') ), 'resource-media-list.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}

	public function learningresource_media_table( $atts ) {

		$args = array(
			'numberposts' => -1,
			'post_type' => 'media_resource',
			'post_status ' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'learning_categories',
					'field'    => 'term_id',
					'terms'    => $_GET['term_id']
				)
			)
		);

		$term = get_term_by('term_id', $_GET['term_id'], 'learning_categories' );

		$posts = get_posts( $args );

		$postArr = array( 
			'posts' => $posts, 
			'term' => $term, 

		);

		if( $_POST ) print_r( $_POST );


		$html = $this->html_content( $postArr, 'resource-media-table.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}

	public function learningresource_media_details( $atts ) {


		$html = $this->html_content( [], 'resource-media-detail.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}


}
