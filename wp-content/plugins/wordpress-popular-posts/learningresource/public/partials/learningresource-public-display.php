<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       www.junefajardo.com
 * @since      1.0.0
 *
 * @package    Learningresource
 * @subpackage Learningresource/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
