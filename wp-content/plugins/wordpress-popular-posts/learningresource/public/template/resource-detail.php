<div class="detail-container">
	<div class="detail-header">
		<p class="pb-2"><?php echo $area; ?> - <?php echo wp_trim_words( get_post_meta( get_the_ID(), 'lrmds-content-topic', true ), 6 ); ?></p>
		<h1><?php the_title(); ?></h1>

		<div class="detail-sub clearfix border-bottom">
			<div class="float-left">
				<p><?php echo $level; ?> | <?php echo strtoupper( get_post_meta( get_the_ID(), 'lrmds-file-extension', true ) ); ?></p>
			</div>
			<div class="float-right detail-downloads">
				<?php if( ! empty( get_post_meta( get_the_ID(), 'lrmds-resource-url' ) ) && ! empty( get_post_meta( get_the_ID(), 'lrmds-file-path' ) ) ) : ?>
					<a href="<?php echo get_post_meta( get_the_ID(), 'lrmds-resource-url', true ); ?>" class="detail-view-file badge badge-pill badge-warning" target="_blank">View</button>
					<a href="javascript:void(0)" class="badge badge-pill badge-warning" id="detail-download-btn">Download</a>
				<?php endif; ?>
			</div>
		</div>

		<div class="details-creation mt-2 clearfix">
			<div class="float-left">
				<span style="opacity: 0.6;"><?php echo 'Publish on '. date( 'Y M jS', strtotime( $post->post_date ) ); ?></span>
			</div>
			<div class="float-right">
				<div class="rating-wrapper" style="min-height: 55px;">
					<?php echo do_shortcode('[ratings id="'. get_the_ID() .'"]'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="details-body">
		<div class="py-2">
			<h4><b>Description</b></h4>
			<?php 
				$content = apply_filters('the_content', $post->post_content );
				$content = str_replace(']]>', ']]&gt;', $content);
				echo $content;
			?>
		</div>
		<div class="py-2">
			<h4><b>Objective</b></h4>
			<?php 
				$content = get_post_meta( get_the_ID(), 'lrmds-objectives', true );
				$content = str_replace(']]>', ']]&gt;', $content);
				echo $content;
			?>
		</div>

		<div class="table-section">
			<hr>
			<h4 class="my-2">Curriculum Information</h4>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>Education Type</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-education-type', true ) ?></td>
					</tr>
					<tr>
						<td>Grade Level</td>
						<td><?php echo get_the_terms( get_the_ID(), 'learning_level' )[0]->name; ?></td>
					</tr>
					<tr>
						<td>Learning Area</td>
						<td><?php echo $area; ?></td>
					</tr>
					<tr>
						<td>Content/Topic</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-content-topic', true ) ?></td>
					</tr>
					<tr>
						<td>Intended Users</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-intended-user', true ) ?></td>
					</tr>
					<tr>
						<td>Competencies</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-competencies', true ) ?></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="table-section>
			<hr>
			<h4 class="my-2">Copyright Information</h4>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>Developer</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-developer', true ) ?></td>
					</tr>
					<tr>
						<td>Copyright</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-copyright', true ) ?></td>
					</tr>
					<tr>
						<td>Copyright Owner</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-copyright-owner', true ) ?></td>
					</tr>
					<tr>
						<td>Conditions of Use</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-condition-of-use', true ) ?></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="table-section>
			<hr>
			<h4 class="my-2">Technical Information</h4>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>File Size</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-file-size', true ) . ' bytes'; ?></td>
					</tr>
					<tr>
						<td>File Type</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-file-type', true ) ?></td>
					</tr>
					<?php if ( ! empty( get_post_meta( get_the_ID(), 'lrmds-operating-system' ) ) ) : ?>
					<tr>
						<td>Operating System</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-operating-system', true ) ?></td>
					</tr>
					<?php endif; ?>
					<?php if ( ! empty( get_post_meta( get_the_ID(), 'lrmds-software-plugin' ) ) ) : ?>
					<tr>
						<td>Software/Plug-in Requirements</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-software-plugin', true ) ?></td>
					</tr>
					<?php endif; ?>
					<?php if ( ! empty( get_post_meta( get_the_ID(), 'lrmds-number-page' ) ) ) : ?>
					<tr>
						<td>No. of Pages</td>
						<td><?php echo get_post_meta( get_the_ID(), 'lrmds-number-page', true ) ?></td>
					</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	jQuery(document).ready(function($) {

		jQuery('#detail-download-btn').on( 'click', function( e ) {

			name = '<?php echo get_post_meta( get_the_ID(),  'lrmds-file-name', true ); ?>';
			extension = '<?php echo get_post_meta( get_the_ID(), 'lrmds-file-extension', true ); ?>';
			path = "<?php echo get_post_meta( get_the_ID(),  'lrmds-resource-url', true ); ?>";
			count = "<?php echo get_post_meta( get_the_ID(),  'lrmds-download-count', true ); ?>";

			console.log( path );
			console.log( extension );

			fetch(path).then(resp => resp.blob()).then(blob => {

			    const url = window.URL.createObjectURL(blob);
			    const a = document.createElement('a');

			    a.style.display = 'none';
			   	a.href = url;
				a.download = name +'.'+ extension;

				document.body.appendChild(a);
				a.click();

				window.URL.revokeObjectURL(url);
			});

			obj = {
				'data' : {
					'ID' : '<?php echo get_the_ID(); ?>',
					'count' : parseInt( count ) + 1 
				},
				'action' : 'download_file'
			}


			jQuery.post(ajax_object.ajax_url, obj, function(response) {
				console.log( response );

				alert( 'Successfully download' );

				location.reload();
			} );


		} ); 
	} );
</script>

