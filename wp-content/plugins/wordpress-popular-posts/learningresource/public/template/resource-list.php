<?php if( isset( $learningarea ) && ! empty( $learningarea ) ) : ?>
<div class="levels-section-list">
	<header class="grade-title clearfix">
		<h1 class="float-left"><?php echo ucwords( $level ); ?></h1>
		<span class="float-left ml-2 badge badge-primary badge-pill"><?php echo $learningarea['total']; ?>
	</header>

	<div class="content-topics-container" id="accordion">
		<ul class="list-group p-0">

			<?php foreach( $learningarea['datas'] as $key => $area ) : ?>

				<li class="border rounded my-1 d-flex justify-content-between flex-column">
					<a href="javascript:void(0)" class="p-2 item-section border-bottom" data-toggle="collapse" data-target="#<?php echo $area['ID']; ?>" aria-expanded="true" aria-controls="<?php echo $area['ID']; ?>">

					<span><?php echo $key; ?></span>
					<span class="badge badge-primary badge-pill float-right"><?php echo $area['rows']; ?></span>
					</a>
				
					<div class="p-2 collapse" aria-labelledby="headingOne" data-parent="#accordion" id="<?php echo $area['ID']; ?>">
						<div class="d-flex flex-column">

							<?php foreach ( $area['content-topics'] as $child => $topics ) : ?>
								<div class="ite-group">

									<a href="subject/<?php echo '?type=' . $type . '&topic=' . base64_encode( $child ) .'&level='. $level; ?>"><?php echo wp_trim_words( $child, 5 ); ?>
										<span class="badge badge-primary badge-pill"><?php echo $topics['size']; ?></span>
									</a>

								</div>
							<?php endforeach; ?>

						</div>
					</div>

				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<?php endif; ?>