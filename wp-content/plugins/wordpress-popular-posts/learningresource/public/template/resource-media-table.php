<div class="media-container">

	<header class="media-header mb-4">
		<h1><?php _e( $term->name ); ?></h1>
		<hr>
		<p><?php _e( $term->description ); ?></p>
	</header>

	<div class="media-filter">
		<form id="form-filter" method="post">
			<div class="btn-group btn-group-toggle" data-toggle="buttons">
				<label class="btn btn-secondary active">
					<input type="radio" name="filter" autocomplete="off" value="photo" checked> <?php _e('Photo'); ?>
				</label>
				<label class="btn btn-secondary">
					<input type="radio" name="filter" autocomplete="off" value="illustaration"> <?php _e('Illustaration'); ?>
				</label>
				<label class="btn btn-secondary">
					<input type="radio" name="filter" autocomplete="off" value="audio"> <?php _e('Audio'); ?>
				</label>
				<label class="btn btn-secondary">
					<input type="radio" name="filter" autocomplete="off" value="video"> <?php _e('Video'); ?>
				</label>
				<label class="btn btn-secondary">
					<input type="radio" name="filter" autocomplete="off" value="all"> <?php _e('All'); ?>
				</label>
			</div>
		</form>
	</div>

	<div class="media-list">
		<div class="d-flex">
		<?php foreach( $posts as $post ) : ?>
			<a href="<?php echo get_permalink( $post->ID ); ?>" class="col-3 my-4 mr-2">
				<?php echo get_the_post_thumbnail( $post->ID, 'thumbnail' ); ?>
			</a>
		<?php endforeach; ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		jQuery('.btn-group').find('input[name="filter"]').on( 'change', function() {
			jQuery('#form-filter').submit();
		} );
	} );
</script>