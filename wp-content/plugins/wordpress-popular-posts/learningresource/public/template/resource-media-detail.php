<div class="media-detail-container">
	<header class="media-hedear mb-4">
		<h1><?php the_title(); ?></h1>
		<p><?php echo get_post_meta( get_the_ID(), 'lrmds-media-type', true ); ?></p>
		<hr>
		<span><?php echo 'Created on '. date( 'Y M jS', strtotime( $post->post_date ) ); ?></span>
	</header>
	<main class="media-body">

		<div class="media-description mb-4">
			<h4>Desciption</h4>
			<?php the_content(); ?>
		</div>

		<?php
			$attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		?>


		<div class="media-list-file">
			<table>	
				<thead>
					<tr>
						<th><?php _e('Preview') ?></th>
						<th><?php _e('Title') ?></th>
						<th><?php _e('Format') ?></th>
						<th><?php _e('Dimension/Duration') ?></th>
						<th><?php _e('File size') ?></th>
						<th><?php _e('Action') ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php echo get_the_post_thumbnail( $post->ID, 'thumbnail', true ); ?>
						</td>
						<td>
							<?php echo get_post_meta( $post->ID, 'lrmds-file-name', true ) .'.'. get_post_meta( $post->ID, 'lrmds-file-extension', true ) ; ?>
						</td>
						<td>
							<?php echo get_post_meta( $post->ID, 'lrmds-file-type', true ); ?>
						</td>
						<td>
							<?php echo $attributes[1] .' x '.  $attributes[2]; ?>
						</td>
						<td>
							<?php echo get_post_meta( $post->ID, 'lrmds-file-size', true ); ?>
						</td>
						<td>
							<a href="javascript:void(0)"><?php _e( 'Download' ); ?></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</main>

</div>