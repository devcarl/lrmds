<div class="list-media-category">
	<h1><?php _e('Media Categories'); ?></h1>
	<ul class="list-group">
		<?php foreach( $categories as $cat ) : ?>
		<li>
			<a href="categories?term_id=<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>