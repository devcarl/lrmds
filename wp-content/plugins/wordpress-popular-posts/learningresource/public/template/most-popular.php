<div class="most-popular">
	<ul class="list-group list-group-flush p-0">
		<?php foreach( $posts as $post ) : ?>
			<li class="list-group-item p-0">
				<a href="<?php echo get_permalink( $post->ID ); ?>" class="most-list-link">
					<header class="header-popular clearfix">
						<div class="float-left">
							<h4><?php _e( $post->post_title ); ?></h4>
							<p><?php _e( $post->post_content ); ?></p>
						</div>
						<div class="float-right">
							<span class="popular-date"><?php echo 'Publish on '. date( 'Y M jS', strtotime( $post->post_date ) ); ?></span>
						</div>
					</header>
					
					<footer class="footer-popular clearfix">
						<div class="float-left">
							<span class="popular-details"><?php echo get_the_terms( $post->ID, 'learning_level' )[0]->name .' | '. get_the_terms( $post->ID, 'learning_area' )[0]->name .' | '. strtoupper( get_post_meta( $post->ID, 'lrmds-file-extension', true ) ); ?>
							</span>
						</div>
						<div class="float-right">
							<span class="badge badge-primary badge-pill"><?php echo get_post_meta( $post->ID, 'lrmds-download-count', true ); ?></span>
						</div>
					</footer>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>