<style type="text/css">
	.media-container a,
	.media-container .media-background {
		height: 200px;
		width: 300px;
		display: block;
		background-repeat: no-repeat;
	}


</style>
<div class="media-container">
	<div class="row">
		<div class="col-6">
			<div class="media-background" style="background-image: url( 'https://lrmds.deped.gov.ph/images/banner1.svg' );">
			</div>
		</div>
		<div class="col-6">
			<div class="row">
				<div class="col-6">
					<a href="" class="" style="background-image: url('https://lrmds.deped.gov.ph/images/banner_photo.png');">
						<span>Photos</span>
					</a>
				</div>
				<div class="col-6">
					<a href="" class="" style="background-image: url('https://lrmds.deped.gov.ph/images/illustration.jpg');">
						<span>Illustration</span>
					</a>
				</div>
				<div class="col-6">
					<a href="" class="" style="background-image: url('https://lrmds.deped.gov.ph/images/video.jpg');">
						<span>Video</span>
					</a>
				</div>
				<div class="col-6">
					<a href="" class="" style="background-image: url('https://lrmds.deped.gov.ph/images/audio.png');">
						<span>Audio</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>