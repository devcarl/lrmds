<div class="resource-sidebar-wrapper border">
	<h4>K to 12 Grade Levels</h4>
	<div class="list-group">
	<?php foreach ( $grouplevel as $key => $val ) : ?>
		<a href="../resources/?level=<?php echo strtolower( $key ); //filter_var( $key, FILTER_SANITIZE_NUMBER_INT); ?>" class="grade-section border-bottom mb-2"><?php echo $key; ?>
		<span class="badge badge-primary badge-pill float-right"><?php echo count( $val ); ?></span>
		</a>
	<?php endforeach; ?>
	</div>
</div>