<?php

/**
 * Fired during plugin activation
 *
 * @link       www.junefajardo.com
 * @since      1.0.0
 *
 * @package    Learningresource
 * @subpackage Learningresource/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Learningresource
 * @subpackage Learningresource/includes
 * @author     juniel <junefajardo94@gmail.com>
 */
class Learningresource_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
