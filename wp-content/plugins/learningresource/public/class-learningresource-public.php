<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.junefajardo.com
 * @since      1.0.0
 *
 * @package    Learningresource
 * @subpackage Learningresource/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Learningresource
 * @subpackage Learningresource/public
 * @author     juniel <junefajardo94@gmail.com>
 */
class Learningresource_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Learningresource_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Learningresource_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( 'datatables-style', plugin_dir_url( __FILE__ ) . 'css/datatables.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootsrap-style', plugin_dir_url( __FILE__ ) . 'css/bootstrap.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/learningresource-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Learningresource_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Learningresource_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'datatables-script', plugin_dir_url( __FILE__ ) . 'js/datatables.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/learningresource-public.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'bootsrap-script', plugin_dir_url( __FILE__ ) . 'js/bootstrap.js', array( 'jquery' ), $this->version, false );

		wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script( $this->plugin_name, 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}
	public function learningresource_download_file(){

		$filedata = $_POST['data'];

		$update = update_post_meta( $filedata['ID'], 'lrmds-download-count', $filedata['count'] );

		wp_send_json_success( $update );

	}

	public function learning_page_template( $template ) {

		global $wp_query;

		if ( $wp_query->query['post_type'] == "learning_resource" ) {
			$template =  dirname( __FILE__ ) . '/template/single-resource.php';
		}

		if ( $wp_query->query['post_type'] == "media_resource" ) {
			$template =  dirname( __FILE__ ) . '/template/single-media.php';
		}

		return $template;
	}

	public function learningresource_upload() {

		if( isset( $_POST['datas'] ) ) {

			$post =  $_POST['datas'];

			$post_type = ( $post['lrmds-education-type'] != 'Media' ) ? 'learning_resource' : 'media_resource';
		
			$postArr = array(
				'post_title' => ( isset( $post['title'] ) ) ? $post['title'] : '',
				'post_content' => ( isset( $post['description'] ) ) ? $post['description'] : '',
				'post_status' => 'publish',
				'post_type' => $post_type,
			);

			unset($post['title']);
			unset($post['description']);

			$postID = wp_insert_post( $postArr );
			// insert new post type

			if( $postID ) {
				// insert category
				foreach( $post as $key => $val ) {
					if( 'lrmds-grade-level' == $key && $post['lrmds-education-type'] != 'Media' ) {
						wp_set_object_terms( $postID, explode( ';', $val ), 'learning_level' );
					} else if( 'lrmds-learning-area' == $key && $post['lrmds-education-type'] != 'Media') {
						wp_set_object_terms( $postID, explode( ';', $val ), 'learning_level' );
					} else if( 'lrmds-media-category' == $key ) {
						wp_set_object_terms( $postID, explode( ';', $val ), 'learning_categories' );
					} else {
						update_post_meta( $postID, $key, $val );
					}
				}
			}
			wp_send_json_success( $postID );
		}

		$ftpname = ['primary','secondary'];

		$imageset = false;

		$returnArr = [];

		if( ! empty( $_FILES['files'] ) && isset( $_POST['postid'] ) ) {

			 $postid = (int)$_POST['postid'];

			foreach( $_FILES['files']['name'] as $key => $filename ) {

				$filePath = wp_upload_bits( $filename, null, file_get_contents(  $_FILES['files']['tmp_name'][$key] ) );

				$ftitle = substr( $filename, 0, strpos( $filename, '.' ) );
				$extension = explode( '/', $_FILES['files']['type'][$key] )[1];
				$fileurlpath = str_replace('\\', '/', $filePath['file'] );

				update_post_meta( $postid, 'lrmds-'. $ftpname[$key] .'-file-name', $ftitle );
				update_post_meta( $postid, 'lrmds-'. $ftpname[$key] .'-file-type', $_FILES['files']['type'][$key] );
				update_post_meta( $postid, 'lrmds-'. $ftpname[$key] .'-file-extension', $extension );
				update_post_meta( $postid, 'lrmds-'. $ftpname[$key] .'-file-size', $_FILES['files']['size'][$key] );
				update_post_meta( $postid, 'lrmds-'. $ftpname[$key] .'-file-url', $filePath['url'] );
				update_post_meta( $postid, 'lrmds-'. $ftpname[$key] .'-file-path', $fileurlpath );

				$returnArr['uploaded'][] = $ftpname[$key];
				$returnArr['filename'][] = $filename;

				if( in_array( $extension,array( 'mp4', 'mov' ) ) ) {
					update_post_meta( $postid, 'lrmds-media-type', 'video' );
				} else if( $extension == 'mp3' ) {
					update_post_meta( $postid, 'lrmds-media-type', 'record' );
				} else if( in_array( $extension, array( 'png','jpg','jpeg', 'gif' ) ) ) {
					update_post_meta( $postid, 'lrmds-media-type', 'image' );

					if( ! $imageset ) {

						$imageFile = $filePath['file'];
						$wp_filetype = wp_check_filetype( $imageFile, null );
						$wp_upload_dir = wp_upload_dir();

						$attachment = array(
							'guid'	=> $wp_upload_dir['url'] . '/' . basename( $imageFile ),
							'post_mime_type' => $wp_filetype['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $imageFile ) ), 
							'post_content' => '', 
							'post_status' => 'inherit',
						);

						$attachId = wp_insert_attachment( $attachment, $imageFile, $postid );

						require_once( ABSPATH . 'wp-admin/includes/image.php' );

						$attach_data = wp_generate_attachment_metadata( $attachId, $imageFile);

						$updatemeta = wp_update_attachment_metadata( $attachId, $attach_data );

						$success = set_post_thumbnail( $postid, $attachId );

						$imageset = true;
					}

				} else {
					update_post_meta( $postid, 'lrmds-media-type', 'file' );
				}
			}

			wp_send_json_success( array( 
				'returnArr' => $returnArr, 
			) );
		}
	}

	public function html_content( $array, $parts ) {

		ob_start();
		extract( $array );
		include 'template/' . $parts;
		$page =  ob_get_contents();
		return ob_get_clean();
	}

	public function term_object_to_array( $object, $find ) {

		for( $x = 0,$newArr = []; $x < count( $object ); $x++ ) {
			if( $find == 'term_id' ) {
				$newArr[] = $object[$x]->term_id;
			}
			if( $find == 'name' ) {
				$newArr[] = $object[$x]->name;
			}
		}

		return $newArr;
	}


	public function learningresource_upload_shortcode() {

		global $pagename;

		// check if it's a login user
		if( ! is_user_logged_in() ) {
			return;
		}

		// check if the user role is valid
		$user = wp_get_current_user();

		if( ! array_intersect( array('editor','administrator','author'), $user->roles ) ) {
			return;
		}

		$gradeLevel = $this->term_object_to_array( get_terms( [ 
			'taxonomy' => 'learning_level', 
			'hide_empty' => false,
		] ), 'name' );

		$learningArea = $this->term_object_to_array( get_terms( [ 
			'taxonomy' => 'learning_area', 
			'hide_empty' => false,
		] ), 'name' );

		$parent_cat = get_terms( [ 
			'taxonomy' => 'learning_categories', 
			'parent' => false,
			'hide_empty' => false,
		] );

		$newCat = [];
	
		foreach( $parent_cat as $child_cat ) {
			$newCat[$child_cat->name] = $this->term_object_to_array( get_terms( [ 
				'taxonomy' => 'learning_categories', 
				'parent' => $child_cat->term_id,
				'hide_empty' => false,
			] ), 'name' );
		}
		//print_r( $newCat ); exit;

		$html = $this->html_content( array(
			'learningArea' => $learningArea,
			'gradeLevel' => $gradeLevel,
			'categories' => $newCat,
		), 'resource-upload.php' );

		return $html;
	}


	public function learningresource_education_sidebar( $atts ) {

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'meta_key' => 'lrmds-education-type',
			'meta_value' => $var['education-type'],
		);

		$posts = get_posts( $args );

		$grouplevel = [];

		foreach( $posts as $post ) {

			$grade = get_the_terms( $post->ID, 'learning_level' );

			if( ! empty( $grade ) ) {

				for( $x = 0; $x < count( $grade ); $x++ ) {
					if( strtolower( $grade[$x]->slug ) != 'basic-literacy' ) {
						$grouplevel[$grade[$x]->name][] = $post->ID;
					}
				}
			}
		}

		$keysArr = array_keys( $grouplevel );

		usort( $keysArr, function( $x, $y ) {

			$x_s = filter_var( $x, FILTER_SANITIZE_NUMBER_INT );
			$y_s = filter_var( $y, FILTER_SANITIZE_NUMBER_INT );

			if( $x_s < $y_s ) {
				return  -1;
			} else {
				return 1;
			}

		} );

		for( $y = 0, $newArr = []; $y < count( $keysArr ); $y++ ) {
			$newArr[$keysArr[$y]] = $grouplevel[$keysArr[$y]];
		}

		$html = $this->html_content( array( 'grouplevel' => $newArr ), 'resource-sidebar.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');	

	}

	public function learningresource_page_render( $atts ) {

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		$level = ( isset( $_GET['level'] ) ) ? $_GET['level'] : 'basic literacy';

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'lrmds-education-type',
					'value' => $var['education-type']
				),
			),
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'learning_level',
					'field' => 'name',
					'terms' => urldecode( $level ),
				)
			)
		);

		$posts = get_posts( $args );

		foreach( $posts as $post ) {

			$area = get_the_terms( $post->ID, 'learning_area' );
			
			if( ! empty( $area ) ) {

				for( $x = 0, $y = 1; $x < count( $area ); $x++ ) {

					$content = get_post_meta( $post->ID, 'lrmds-content-topic', true );

					if( ! empty( $content ) ) {

						$newArray = array(
							'ID' => $post->ID,
							'title' => $post->post_title
						);

						$learningarea['total'] += $y;
						$learningarea['datas'][$area[$x]->slug]['ID'] = $area[$x]->term_id;
						$learningarea['datas'][$area[$x]->slug]['rows'] += $y;
						$learningarea['datas'][$area[$x]->slug]['content-topics'][$content]['size'] += $y;
						$learningarea['datas'][$area[$x]->slug]['content-topics'][$content]['data'][] = $newArray;
					}
				}
			}
		}

		$html = $this->html_content(
			array(
				'learningarea' => $learningarea,
				'type' => $var['education-type'],
				'level' => $level
			), 'resource-list.php'
		);

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');	
	}

	public function learningresource_most_popular( $atts ) {

		$var = shortcode_atts( array(
			'post-type' => '',
			'education-type' => '',
		), $atts );

		$args = array(
			'numberposts' => -1,
			'post_type' => $var['post-type'],
			'post_status ' => 'publish',
			'order' => 'DESC',
			'meta_query' => array(
				array(
					'key' => 'lrmds-download-count',
					'value' => 'NaN',
					'compare' => '!='
				),

			)
		);

		if( isset( $var['education-type'] ) ) {
			$args['meta_query'][] = array(
				'key' => 'lrmds-education-type',
				'value' => $var['education-type']
			);
		}

		$posts = get_posts( $args );

		$html = $this->html_content( array( 'posts' => $posts ), 'most-popular.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');

	}
	public function learningresource_related_resource( $atts ) {

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		$levelArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_level' ), 'term_id' );
		$areaArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_area' ), 'term_id' );

		$args = array(
			'numberposts' => -1,
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'exclude'	=> array( get_the_ID() ),
			'meta_query' => array(
				array(
					'key' => 'lrmds-education-type',
					'value' => $var['education-type']
				),
			)
		);

		if( ! empty( $levelArr ) ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'learning_level',
				'field'    => 'term_id',
				'terms'    => $levelArr
			);
		}

		if( ! empty( $areaArr ) ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'learning_area',
				'field'    => 'term_id',
				'terms'    => $areaArr
			);
		}

		$posts = get_posts( $args );

		$html = $this->html_content( array( 'posts' => $posts ), 'related-resource.php' );

		return $html;
	} 

	public function learningresource_topic_list( $atts ) {

		$option_val = get_option( 'learning_options' );

		$var = shortcode_atts( array(
			'education-type' => '',
		), $atts );

		$type = ( isset( $_GET['type'] ) ) ?  $_GET['type'] : $var['education-type'];
		$level = ( $var['education-type'] != 'K to 12' && ! isset( $_GET['level'] ) ) ? ( isset( $_POST['filter'] ) ) ? $_POST['filter'] : 'basic literacy' : $_GET['level'];
		$topic = base64_decode( $_GET['topic'] );
		$section = ( isset( $_POST['filter'] ) ) ? 	$_POST['filter'] : 'all';

		$args = array(
			'post_type' => 'learning_resource',
			'post_status ' => 'publish',
			'posts_per_page' => 30,
			'meta_query' => array(
				array(
					'key' => 'lrmds-education-type',
					'value' => $type
				)
			)
		);

		if( isset( $_GET['topic'] ) ) {
			$args['meta_query'][] = array(
				'key' => 'lrmds-content-topic',
				'value' => $topic,
				'compare' => '=',
			);
		}

		if( $var['education-type'] == 'K to 12' && $section != 'all' ) {
			$args['meta_query'][] = array(
				'key' => 'lrmds-education-section',
				'value' => $_POST['filter'],
				'compare' => '=',
			);
		}

		if( isset( $level ) ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'learning_level',
					'field' => 'name',
					'terms' => $level
				)
			);
		}

		$thepost = new WP_Query( $args );

		$current = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		$pagination = array(
            'format'  => '?paged=%#%',
            'current' => $current,
            'total'   => $thepost->max_num_pages,
        );

		$html = $this->html_content( array( 
			'posts' => $thepost->posts, 
			'level' => $level, 
			'type' => $type,
			'topic' => $topic,
			'section' => $section,
			'count' => $thepost->found_posts,
			'pagination' => paginate_links( $pagination )
		), 'resource-table.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');

	}

	public function learningresource_details( $atts ) {

		$levelArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_level' ), 'name' );
		$areaArr = $this->term_object_to_array( get_the_terms( get_the_ID(), 'learning_area' ), 'name' );

		$html = $this->html_content( 
			array( 
				'level' => implode( '|', $levelArr ),
				'area' =>  implode( '|', $areaArr )
			), 
			'resource-detail.php' 
		);

		return $html;
	}

	public function learningresource_media( $atts ) {

		$html = $this->html_content( [], 'resource-media.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}

	public function learningresource_media_list( $atts ) {

		$args = array(
			'taxonomy' => 'learning_categories',
			'parent' => false,
		);

		if( isset( $_GET['term_id'] ) ) {
			$args['parent'] = $_GET['term_id'];
		}

		$categories = get_terms( $args );

		$catArr = array( 
			'categories' => $categories,
			'subterm' => $_GET['term_id']
		);

		$html = $this->html_content( $catArr, 'resource-media-list.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}

	public function learningresource_media_table( $atts ) {

		$var = shortcode_atts( array(
			'type' => '',
		), $atts );

		$meta_value = ( isset( $_POST['filter'] ) ) ? $_POST['filter'] : ( ! empty( $var['type'] ) ) ? $var['type'] : 'all';

		$current = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		$args = array(
			'post_type' => 'media_resource',
			'post_status ' => 'publish',
			'posts_per_page' => 30,
			'paged'			=> $current
		);

		if( isset( $_GET['term_id'] ) ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'learning_categories',
				'field'    => 'term_id',
				'terms'    => $_GET['term_id']
			);
		}

		if( $meta_value != 'all' ) {
			$args['meta_query'][] = array(
				'key' => 'lrmds-attachment-type',
				'value' => $meta_value
			);
		}

		if( empty( $var['type'] ) && isset( $_GET['term_id'] ) ) {
			$term = get_term_by('term_id', $_GET['term_id'], 'learning_categories' );
		}

		$posts = new WP_Query( $args );

		$pagination = array(
            'format'  => '?paged=%#%',
            'current' => $current,
            'total'   => $posts->max_num_pages,
        );

		$postArr = array( 
			'posts' => $posts, 
			'term' => $term,
			'type' => ( ! empty( $var['type'] ) ) ? $var['type'] : '',
			'filter' => $meta_value,
			'count' => $posts->found_posts,
			'pagination' => paginate_links( $pagination )
		);

		$html = $this->html_content( $postArr, 'resource-media-table.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}

	public function learningresource_media_details( $atts ) {


		$html = $this->html_content( [], 'resource-media-detail.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');
	}

	public function learningresource_audio_list( $atts ) {

		$var = shortcode_atts( array(
			'media-type' => '',
		), $atts );

		$args = array(
			'post_type' => 'media_resource',
			'post_status' => 'publish',
			'meta_key' => 'lrmds-attachment-type',
			'meta_value' => $var['media-type']
		);

		$posts = new WP_Query( $args );

		$pagination = array(
            'format'  => '?paged=%#%',
            'current' => $current,
            'total'   => $posts->max_num_pages,
        );

		$postArr = array( 
			'posts' => $posts, 
			'count' => $posts->found_posts,
			'pagination' => paginate_links( $pagination )
		);

		$html = $this->html_content( $postArr, 'resource-media-table.php' );

		return do_shortcode('[et_pb_section]'. $html .'[/et_pb_section]');

	}

	public function convert_size( $size ) {

		$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		$power = $size > 0 ? floor(log($size, 1024)) : 0;
		return number_format($size / pow(1024, $power), 0, '.', ',') . ' ' . $units[$power];
	}

	public function register_form( $atts ) {
		return $this->html_content( array(), 'registration-form.php' );
	}
}
