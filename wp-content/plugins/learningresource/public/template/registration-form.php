<style type="text/css">
	.lrmds-form {

	}

	.lrmds-section {
		display: -ms-flexbox;
		display: flex;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		margin-bottom: 16px;
	}

	.lrmds-section:last-child {
		margin-bottom: 0;
	}

	.lrmds-section-title,
	.lrmds-field-title {
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 10px;
		flex: 0 0 100%;
		max-width: 100%;
	}

	.lrmds-field,
	.lrmds-field-group {
		display: -ms-flexbox;
		display: flex;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		flex: 0 0 100%;
		max-width: 100%;
		margin-left: -8px;
		margin-right: -8px;
	}

	.lrmds-field-item {
		flex: 0 0 100%;
		max-width: 100%;
		padding: 0 8px;
		margin-bottom: 10px;
	}

	.lrmds-field-item input,
	.lrmds-field-item select {
		width: 100%;
		padding: 5px 7px;
		border-radius: 3px;
	}

	.lrmds-field-group-2 .lrmds-field-item {
		flex: 0 0 50%;
		max-width: 50%;
	}

	.lrmds-field-group-3 .lrmds-field-item {
		flex: 0 0 33.333333%;
		max-width: 33.333333%;
	}
</style>
<?php

$gender = array(
	'male' 		=> esc_html__( 'Male', 'learningresource' ),
	'female' 	=> esc_html__( 'Female', 'learningresource' ),
);

$Affiliation = array(
	'region' 	=> esc_html__( 'Region', 'learningresource' ),
	'division' 	=> esc_html__( 'Division', 'learningresource' ),
	'district' 	=> esc_html__( 'District', 'learningresource' ),
	'school' 	=> esc_html__( 'School', 'learningresource' ),
);

?>
<form class="lrmds-form" method="post">
	<div class="lrmds-section">
		<p class="lrmds-section-title"><?php esc_html_e( 'Full Name', 'learningresource' ); ?></p>
		<div class="lrmds-field-group lrmds-field-group-3">
			<span class="lrmds-field-item">
				<input type="text" name="last_name" placeholder="<?php esc_html_e( 'Last Name', 'learningresource' ); ?>" />
			</span>
			<span class="lrmds-field-item">
				<input type="text" name="middle_name" placeholder="<?php esc_html_e( 'Middle Name', 'learningresource' ); ?>" />
			</span>
			<span class="lrmds-field-item">
				<input type="text" name="first_name" placeholder="<?php esc_html_e( 'First Name', 'learningresource' ); ?>" />
			</span>
		</div>
	</div>
	<div class="lrmds-section">
		<p class="lrmds-section-title"><?php esc_html_e( 'E-mail Address', 'learningresource' ); ?></p>
		<div class="lrmds-field">
			<span class="lrmds-field-item">
				<input type="email" name="email" placeholder="<?php esc_html_e( 'Email', 'learningresource' ); ?>" />
			</span>
		</div>
	</div>
	<div class="lrmds-section">
		<p class="lrmds-section-title"><?php esc_html_e( 'Username', 'learningresource' ); ?></p>
		<div class="lrmds-field">
			<span class="lrmds-field-item">
				<input type="text" name="username" placeholder="<?php esc_html_e( 'Username', 'learningresource' ); ?>" />
			</span>
		</div>
	</div>
	<div class="lrmds-section">
		<div class="lrmds-field-group lrmds-field-group-2">
			<span class="lrmds-field-item">
				<p class="lrmds-field-title"><?php esc_html_e( 'Password', 'learningresource' ); ?></p>
				<input type="password" name="password" placeholder="<?php esc_html_e( 'Password', 'learningresource' ); ?>" />
			</span>
			<span class="lrmds-field-item">
				<p class="lrmds-field-title"><?php esc_html_e( 'Repeat Password', 'learningresource' ); ?></p>
				<input type="password" name="repeat_password" placeholder="<?php esc_html_e( 'Repeat Password', 'learningresource' ); ?>" />
			</span>
		</div>
	</div>
	<div class="lrmds-section">
		<div class="lrmds-field-group lrmds-field-group-2">
			<span class="lrmds-field-item">
				<p class="lrmds-field-title"><?php esc_html_e( 'Birth Date', 'learningresource' ); ?></p>
				<input type="text" name="birth_date" placeholder="<?php esc_html_e( 'Birth Date', 'learningresource' ); ?>" />
			</span>
			<span class="lrmds-field-item">
				<p class="lrmds-field-title"><?php esc_html_e( 'Gender', 'learningresource' ); ?></p>
				<select>
					<option value="" selected="selected"><?php esc_html_e( 'Select Gender', 'learningresource' ); ?></option>
					<?php foreach ( $gender as $key => $sex ) : ?>
						<option value="<?php echo esc_attr( $key ); ?>"><?php echo $sex; ?></option>
					<?php endforeach; ?>
				</select>
			</span>
		</div>
	</div>
	<div class="lrmds-section">
		<div class="lrmds-field-group lrmds-field-group-2">
			<span class="lrmds-field-item">
				<p class="lrmds-field-title"><?php esc_html_e( 'Affiliation', 'learningresource' ); ?></p>
				<select>
					<option selected="" value="191">Region</option>
                    <option value="192">Division</option>
                    <option value="193">District</option>
                    <option value="0">School</option>
				</select>
			</span>
			<span class="lrmds-field-item">
				<p class="lrmds-field-title"><?php esc_html_e( 'Region', 'learningresource' ); ?></p>
				<select>
					<option value="" selected="selected">Select Region</option>
					<option value="3">Central Office</option>
					<option value="6501">School Abroad</option>
					<option value="1">Region I - Ilocos Region</option>
					<option value="2">Region II - Cagayan Valley</option>
					<option value="4">Region III - Central Luzon</option>
					<option value="5">Region IV-A (CALABARZON)</option>
					<option value="7">Region V - Bicol Region</option>
					<option value="8">Region VI - Western Visayas</option>
					<option value="9">Region VII - Central Visayas</option>
					<option value="10">Region VIII - Eastern Visayas</option>
					<option value="11">Region IX - Zamboanga Peninsula</option>
					<option value="12">Region X - Northern Mindanao</option>
					<option value="13">Region XI - Davao Region</option>
					<option value="14">Region XII - SOCCSKSARGEN</option>
					<option value="18">NCR</option>
					<option value="17">CAR</option>
					<option value="16">BARMM</option>
					<option value="15">Region XIII (Caraga)</option>
					<option value="6">MIMAROPA Region</option>
					<option value="70">NEGROS ISLAND REGION (NIR)</option>
				</select>
			</span>
		</div>
	</div>
</form>