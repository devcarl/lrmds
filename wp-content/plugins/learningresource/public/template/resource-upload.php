<div class="learning-resource-upload-wrapper">
	<form id="lrmds-form-upload" method="post" action="<?php the_permalink(); ?>" enctype="multipart/form-data">
		<div class="form-group py-2">
			<label for="title"><?php _e( 'Title'); ?></label>
			<input class="form-control" id="title" type="text" name="title" required="true">
		</div>

		<div class="form-group">
			<label for="education">Education Type</label>
			<select id="education" name="lrmds-education-type" class="form-control">
				<option value=""><?php _e( '--Select--'); ?></option>
				<option value="K to 12"><?php _e( 'K to 12'); ?></option>
				<option value="Alternative Learning System"><?php _e( 'Alternative Learning System'); ?></option>
				<option value="Professional Development"><?php _e( 'Professional Development'); ?></option>
				<option value="Media"><?php _e( 'Media'); ?></option>
			</select>
		</div>

		<div class="form-group">
			<label for="description"><?php _e( 'Description'); ?></label>
			<?php wp_editor( '', 'description', array( 'media_buttons' => false, 'textarea_rows' => '5' ) ) ?>
		</div>

		<div class="is-education-wrapper">
			<div class="form-group">
				<label for="objective"><?php _e( 'Objective'); ?></label>
				<?php wp_editor( '', 'lrmds-objectives', array( 'media_buttons' => false, 'textarea_rows' => '5' ) ) ?>
			</div>
		</div>

		<div class="is-media-wrapper mediatype">
			<div class="form-group">
				<label for="attachment">Attachment Type</label>
				<select id="attachment" name="lrmds-attachment-type" class="form-control">
					<option value=""><?php _e( '--Select--'); ?></option>
					<option value="photo"><?php _e( 'Photo'); ?></option>
					<option value="illustration"><?php _e( 'Illustration'); ?></option>
					<option value="video"><?php _e( 'Video'); ?></option>
					<option value="audio"><?php _e( 'Audio'); ?></option>
				</select>
			</div>
		</div>

		<div class="form-group mediaupload">
			<label for="lrmds-primary-file"><?php _e('Primary Upload')  ?></label><br>
			<input required="true" type="file" name="lrmds-primary-file" id="lrmds-primary-file">
		</div>

		<div class="form-group mediaupload">
			<label for="lrmds-secondary-file"><?php _e('Secondary Upload')  ?></label><br>
			<input required="true" type="file" name="lrmds-secondary-file" id="lrmds-secondary-file">
		</div>

		<div class="is-education-wrapper">
			<div class="form-group">
				<label for="resourcetype"><?php _e( 'Resource Type'); ?></label>
				<input required="true" class="form-control" name="lrmds-resource-type" id="resourcetype" type="text">
			</div>
		</div>

		<div class="is-education-wrapper">
			<div class="form-row">
				<div class="form-group col-6">
					<label for="condition-use"><?php _e( 'Operating System'); ?></label>
					<input required="true" type="text" class="form-control" name="lrmds-operating-system">
				</div>

				<div class="form-group col-6">
					<label for="condition-use"><?php _e( 'Software/Plug-in Requirements'); ?></label>
					<input required="true" type="text" class="form-control" name="lrmds-software-plugin">
				</div>

				<div class="form-group col-6">
					<label for="condition-use"><?php _e( 'No. of Pages'); ?></label>
					<input required="true" type="text" class="form-control" name="lrmds-number-page">
				</div>

				<div class="form-group col-6">
					<label for="language"><?php _e( 'Language'); ?></label>
					<input required="true" class="form-control" name="lrmds-language-type" id="language" type="text">
				</div>
			</div>
		</div>

		<div class="is-education-wrapper">
			<div class="form-group">
				<label for="education-section"><?php _e( 'Section'); ?></label>
				<select id="education-section" name="lrmds-education-section" class="form-control">
					<option value=""><?php _e( '--Select--'); ?></option>
					<option value="sped"><?php _e( 'SPED'); ?></option>
					<option value="madrasah"><?php _e( 'Madrasah'); ?></option>
					<option value="ip education"><?php _e( 'IP Eductaion' ); ?></option>
				</select>
			</div>

			<div class="form-group">
				<label for="grede_level"><?php _e( 'Grade Level'); ?></label>
				<div class="form-checkboxes-group" id="grede_level">
					<div class="row">
					<?php foreach ( $gradeLevel as $key => $grade ) : ?>
						<div class="col col-lg-4">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" name="lrmds-grade-level" class="custom-control-input" id="grade-item-<?php echo "{$key}"; ?>" value="<?php echo $grade; ?>">
								<label class="custom-control-label" for="grade-item-<?php echo "{$key}"; ?>"><?php echo __( $grade ); ?></label>
							</div>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="learningarea"><?php _e( 'Learning Area' ); ?></label>
				<div class="form-checkboxes-group" id="learningarea">
					<div class="row">
					<?php foreach ( $learningArea as $key => $learn ) : ?>
						<div class="col col-lg-4">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" name="lrmds-learning-area" class="custom-control-input" id="learning-item-<?php echo "{$key}"; ?>" value="<?php echo $learn; ?>">
								<label class="custom-control-label" for="learning-item-<?php echo "{$key}"; ?>"><?php echo __( $learn ); ?></label>
							</div>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>

		<div class="mediacategories">
			<div class="form-group">
				<label for="mediacategory"><?php _e( 'Media Categories' ); ?></label>
				<div class="form-checkboxes-group row" id="mediacategory">
					<?php foreach ( $categories as $key => $cat ) : ?>
						<div class="my-3 col-3">
							<div class="media-category">
								<div class="media-parent">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" name="lrmds-media-category" class="custom-control-input" id="media-item-<?php echo strtolower( str_replace( ' ', '-', $key ) ); ?>" value="<?php echo $key; ?>">
										<label class="custom-control-label" for="media-item-<?php echo strtolower( str_replace( ' ', '-', $key ) ); ?>"><?php echo __( $key ); ?></label>
									</div>
								</div>
								<?php if( ! empty( $cat ) ) : ?>
									<div class="child-group">
										<?php foreach( $cat as $i ) : ?>
											<div class="child-item">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" name="lrmds-media-category" class="custom-control-input" id="media-item-<?php echo strtolower( str_replace( ' ', '-', $key .'-'. $i  ) ); ?>" value="<?php echo $i; ?>">
													<label class="custom-control-label" for="media-item-<?php echo strtolower( str_replace( ' ', '-', $key .'-'. $i  ) ); ?>"><?php echo __( $i ); ?></label>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<div class="is-education-wrapper">
			<div class="form-group">
				<label for="content_or_topic"><?php _e( 'Content/Topic'); ?></label>
				<textarea class="form-control" name="lrmds-content-topic" id="content_or_topic"></textarea>
			</div>

			<div class="form-group">
				<label for="competencies"><?php _e( 'Competencies'); ?></label>
				<textarea class="form-control" id="competencies" name="lrmds-competencies"></textarea>
			</div>

			<div class="form-group py-2">
				<label for="intended_users"><?php _e( 'Intended Users'); ?></label>
				<input required="true" class="form-control" id="intended_users" type="text" name="lrmds-intended-users">
			</div>

			<div class="form-group">
				<label for="copyright"><?php _e( 'Copyright'); ?></label>
				<div id="copyright">
					<div class="custom-control custom-radio">
					  <input required="true" type="radio" id="yes" name="lrmds-copyright" class="custom-control-input" value="Yes">
					  <label class="custom-control-label" for="yes"><?php _e('Yes'); ?></label>
					</div>
					<div class="custom-control custom-radio">
					  <input required="true" type="radio" id="no" name="lrmds-copyright" class="custom-control-input" value="No">
					  <label class="custom-control-label" for="no"><?php _e('No'); ?></label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="copyright-owner"><?php _e( 'Copyright Owner'); ?></label>
				<input required="true" type="text" class="form-control" name="lrmds-copyright-owner">
			</div>

			<div class="form-group">
				<label for="condition-use"><?php _e( 'Condition of Use'); ?></label>
				<div class="custom-control custom-checkbox">
				  <input type="checkbox" id="for-pulic-use" name="lrmds-condition-of-use" class="custom-control-input" value="For public use">
				  <label class="custom-control-label" for="for-pulic-use"><?php _e('For public use'); ?></label>
				</div>
				<div class="custom-control custom-checkbox">
				  <input type="checkbox" id="copy" name="lrmds-condition-of-use" class="custom-control-input" value="Copy">
				  <label class="custom-control-label" for="copy"><?php _e('Copy'); ?></label>
				</div>
				<div class="custom-control custom-checkbox">
				  <input type="checkbox" id="read" name="lrmds-condition-of-use" class="custom-control-input" value="Read">
				  <label class="custom-control-label" for="read"><?php _e('Read'); ?></label>
				</div>
				<div class="custom-control custom-checkbox">
				  <input type="checkbox" id="print" name="lrmds-condition-of-use" class="custom-control-input" value="Print">
				  <label class="custom-control-label" for="print"><?php _e('Print'); ?></label>
				</div>
			</div>
		</div>

		<div class="form-group">
			<input required="true" class="btn btn-primary" type="submit" value="Submit" name="submit">
		</div>
	</form>

</div>

<script type="text/javascript">

	jQuery(document).ready(function($) {

		jQuery( '#education' ).on( 'change', function( e ) {

			if( jQuery(this).val() != '' ) {

				jQuery('.mediaupload').show();

				if( jQuery(this).val() != 'Media' ) {

					jQuery('.is-education-wrapper').show();
					jQuery('.is-education-wrapper').find('input[type="text"]').val('');
					jQuery('.is-education-wrapper').find('input[type="text"]').attr('required','true');
				} else {

					jQuery('.is-education-wrapper').hide();
					jQuery('.is-education-wrapper').find('input[type="text"]').val('');
					jQuery('.is-education-wrapper').find('input[type="text"]').removeAttr('required');
					jQuery('.is-education-wrapper').find('input[type="radio"], input[type="checkbox"]').removeAttr('required');
				}

				if( jQuery(this).val() == 'Media' ) {

					jQuery('.is-media-wrapper, .mediacategories').show();
					jQuery('.is-media-wrapper').find('input[type="text"]').val('');
					jQuery('.is-media-wrapper').find('input[type="text"]').attr('required','true');

				} else {

					jQuery('.is-media-wrapper, .mediacategories').hide();
					jQuery('.is-media-wrapper').find('input[type="text"]').val('');
					jQuery('.is-media-wrapper').find('input[type="text"]').removeAttr('required');
					jQuery('.is-media-wrapper').find('input[type="radio"], input[type="checkbox"]').removeAttr('checked');
				}
			} else {
				jQuery('.is-education-wrapper, .is-media-wrapper, .mediaupload, .mediacategories').hide();
				jQuery('.is-education-wrapper, .is-media-wrapper').find('input[type="text"]').val('');
				jQuery('.is-education-wrapper, .is-media-wrapper').find('input[type="text"]').removeAttr('required');
				jQuery('.is-education-wrapper, .is-media-wrapper').find('input[type="radio"], input[type="checkbox"]').removeAttr('checked');
			}
		} );

		jQuery( '#attachment' ).on( 'change', function() {

			if( jQuery(this).val() != '' ) {

				jQuery('.mediaupload').show();

				if( jQuery(this).val() == 'audio' || jQuery(this).val() == 'video'  ) {
					jQuery('.mediacategories').hide();
				} else {
					jQuery('.mediacategories').show();
				}
			} else {
				jQuery('.mediaupload, .mediacategories').hide();
			}
		});

		function groupBy(array, key, fn) {

		   var hash = Object.create( null );

			return array.reduce( function ( r, o ) {
				if (!hash[o[key]]) {
					hash[o[key]] = {};
					hash[o[key]][key] = o[key];
					r.push(hash[o[key]]);
				}

				fn(o, hash[o[key]]);
				return r;
			}, [] );
		}

		jQuery( '#lrmds-form-upload' ).submit( function( e ) {

			e.preventDefault();

			var editor = tinymce.editors;

			var result = groupBy( jQuery( this ).serializeArray(), 'name', function (s, t) {
				t.value = t.value ? t.value + ';' + s.value : s.value;
			});

			for( var x = 0; x < editor.length; x++ ) {
				result.push( { 'name' : editor[x].id, 'value' : editor[x].getContent() } );
			}

			for( var y = 0, newarr = []; y < result.length; y++ ) {
				newarr[result[y]['name']] = result[y]['value'];
			}
			var form_data = new FormData();

			var inputfiles = jQuery( '#lrmds-primary-file, #lrmds-secondary-file' );
				
			jQuery.each( inputfiles, function( i, e ) {
				form_data.append('files['+ i +']', jQuery(this).prop('files')[0]);
			} );

			form_data.append('action', 'learningresource_upload');

			var	obj = {
				'datas' : Object.assign( {}, newarr ),
				'action' : 'learningresource_upload'
			}

			jQuery.post(ajax_object.ajax_url, obj, function(postid) {
	
				form_data.append('postid', postid['data'] );

				jQuery.ajax({
					url: ajax_object.ajax_url,
					type: 'POST',
					contentType: false,
					processData: false,
					data: form_data,
					success: function ( response ) {
						console.log( response );
						setTimeout( function() {
							location.reload();
						}, 500 );

					}
				});
			});
		});	
	});

</script>