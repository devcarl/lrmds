<div class="media-detail-container">
	<header class="media-hedear mb-4">
		<h1><?php the_title(); ?></h1>
		<p><?php echo ucfirst( get_post_meta( get_the_ID(), 'lrmds-attachment-type', true ) ); ?></p>
		<hr>
		<span><?php echo 'Created on '. date( 'Y M jS', strtotime( $post->post_date ) ); ?></span>
	</header>
	<main class="media-body">

		<div class="media-description mb-4">
			<h4>Desciption</h4>
			<?php the_content(); ?>
		</div>

		<?php
			$attributes = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
		?>

		<div class="media-list-file">
			<table>	
				<thead>
					<tr>
						<th><?php _e('Preview') ?></th>
						<th><?php _e('Title') ?></th>
						<th><?php _e('Format') ?></th>
						<th><?php _e('Dimension/Duration') ?></th>
						<th><?php _e('File size') ?></th>
						<th><?php _e('Action') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if( ! empty( get_post_meta( get_the_ID(), 'lrmds-primary-file-name' ) ) ) : ?>
					<tr>
						<td>
							<?php if( 'audio' == get_post_meta( get_the_ID(), 'lrmds-attachment-type', true ) ) : ?>
								<audio controls>
									<source src="<?php echo get_post_meta( get_the_ID(), 'lrmds-primary-file-url', true ); ?>" type="audio/mpeg"></source>
								</audio>
							<?php elseif ( 'video' == get_post_meta( get_the_ID(), 'lrmds-attachment-type', true )) : ?>
								<video controls>
									<source src="<?php echo get_post_meta( get_the_ID(), 'lrmds-primary-file-url', true ); ?>"></source>
								</video>
							<?php else : ?>
								<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'thumbnail', true ); ?>" width="80" height="80">
							<?php endif; ?>
						</td>
						<td>
							<?php echo get_post_meta( get_the_ID(), 'lrmds-primary-file-name', true ) .'.'. get_post_meta( get_the_ID(), 'lrmds-primary-file-extension', true ) ; ?>
						</td>
						<td>
							<?php echo get_post_meta( get_the_ID(), 'lrmds-primary-file-type', true ); ?>
						</td>
						<td>
							<?php echo $attributes[1] .' x '.  $attributes[2]; ?>
						</td>
						<td>
							<?php echo $this->convert_size( get_post_meta( get_the_ID(), 'lrmds-primary-file-size', true ) ); ?>
						</td>
						<td>
							<a class="download-btn primary" href="javascript:void(0)"><?php _e( 'Download' ); ?></a>
						</td>
					</tr>
					<?php endif; ?>
					<?php if( ! empty( get_post_meta( get_the_ID(), 'lrmds-secondary-file-name' ) ) ) : ?>
						<tr>
							<td>
								<img src="<?php echo get_post_meta( get_the_ID(), 'lrmds-secondary-file-url', true ); ?>" width="80" height="80"> 
							</td>
							<td>
								<?php echo get_post_meta( get_the_ID(), 'lrmds-secondary-file-name', true ) .'.'. get_post_meta( get_the_ID(), 'lrmds-secondary-file-extension', true ) ; ?>
							</td>
							<td>
								<?php echo get_post_meta( get_the_ID(), 'lrmds-secondary-file-type', true ); ?>
							</td>
							<td>
								<?php echo $attributes[1] .' x '.  $attributes[2]; ?>
							</td>
							<td>
								<?php echo $this->convert_size( get_post_meta( get_the_ID(), 'lrmds-secondary-file-size', true ) ); ?>
							</td>
							<td>
								<a class="download-btn secondary" href="javascript:void(0)"><?php _e( 'Download' ); ?></a>
							</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</main>

</div>


<script type="text/javascript">
	
	jQuery(document).ready(function($) {

		jQuery('.download-btn.primary, .download-btn.secondary').on( 'click', function( e ) {

			var name, extension, path, count;

			if( jQuery(this).hasClass( 'primary' ) ) {
				name = '<?php echo get_post_meta( get_the_ID(),  'lrmds-primary-file-name', true ); ?>';
				extension = '<?php echo get_post_meta( get_the_ID(), 'lrmds-primary-file-extension', true ); ?>';
				path = "<?php echo get_post_meta( get_the_ID(),  'lrmds-primary-file-url', true ); ?>";
				count = "<?php echo get_post_meta( get_the_ID(),  'lrmds-download-count', true ); ?>";
			} else {
				name = '<?php echo get_post_meta( get_the_ID(),  'lrmds-secondary-file-name', true ); ?>';
				extension = '<?php echo get_post_meta( get_the_ID(), 'lrmds-secondary-file-extension', true ); ?>';
				path = "<?php echo get_post_meta( get_the_ID(),  'lrmds-secondary-file-url', true ); ?>";
				count = "<?php echo get_post_meta( get_the_ID(),  'lrmds-download-count', true ); ?>";
			}

			fetch(path).then(resp => resp.blob()).then(blob => {

			    const url = window.URL.createObjectURL(blob);
			    const a = document.createElement('a');

			    a.style.display = 'none';
			   	a.href = url;
				a.download = name +'.'+ extension;

				document.body.appendChild(a);
				a.click();

				window.URL.revokeObjectURL(url);
			});

			var obj = {
				'data' : {
					'ID' : '<?php echo get_the_ID(); ?>',
					'count' : parseInt( count ) + 1 
				},
				'action' : 'download_file'
			}


			jQuery.post(ajax_object.ajax_url, obj, function(response) {
				console.log( response );

				alert( 'Successfully download' );

				location.reload();
			} );


		} ); 
	} );
</script>