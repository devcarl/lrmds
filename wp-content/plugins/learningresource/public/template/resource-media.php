<div class="media-container">

	<ul class="list-inline p-0">
		<li class="list-inline-item">
			<a href="photo" class="">
				<img src="https://lrmds.deped.gov.ph/images/banner_photo.png" width="250" height="250">
				<span>Photos</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="illustrations" class="">
				<img src="https://lrmds.deped.gov.ph/images/illustration.jpg" width="250" height="250">
				<span>Illustration</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="videos" class="">
				<img src="https://lrmds.deped.gov.ph/images/video.jpg" width="250" height="250">
				<span>Video</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="audios" class="">
				<img src="https://lrmds.deped.gov.ph/images/audio.png" >
				<span>Audio</span>
			</a>
		</li>
	</ul>
</div>