<div class="resource-sidebar-wrapper list-media-category border">
	<h4><?php echo ( ! isset( $subterm ) ) ? 'Categories' : 'Subcategories'; ?></h4>
	<div class="list-group">
	<?php foreach( $categories as $cat ) : ?>
		<a href="categories?term_id=<?php echo $cat->term_id; ?>" class="grade-section border-bottom mb-2"><?php echo $cat->name; ?></a>
	<?php endforeach; ?>
	</div>
</div>