<div class="resource-table">
	<header class="resource-table-header">

		<?php if( $type == 'K to 12' ) : ?>
		<p class="pb-4"><span style="opacity: 0.6;"><?php echo ucfirst( $level ); ?> / </span><?php echo $type .' -- '. $topic; ?></p>
		<?php endif; ?>

		<?php if( $type == 'Alternative Learning System' ) : ?>
		<h2 class="pb-4"><span style="opacity: 0.6;color:#049828;"><?php _e('ALS Resources'); ?> | </span><small><?php echo ucfirst( $level ); ?></small></h2>
		<?php endif; ?>

		<?php if( $type == 'Professional Development' ) : ?>
		<h2 class="pb-4"><span style="opacity: 0.6;color:#049828;"><?php _e('Professional Development Resources'); ?></span></h2>
		<?php endif; ?>

	</header>
	<div class="resource-filter clearfix">
		<div class="float-left">
			<?php if( $type == 'Alternative Learning System' ) : ?>
			<form method="post" class="resource-form-filter pb-4" id="resource-form">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-default <?php echo ( $section == 'elementary' ) ? ' active' : ''; ?>">
						<input type="radio" name="filter" autocomplete="off" value="elementary" <?php echo ( $section == 'elementary') ? 'checked' : ''; ?> /><?php _e('Elementary'); ?>
					</label>
					<label class="btn btn-default <?php echo ( $section == 'secondary' ) ? ' active' : ''; ?>">
						<input type="radio" name="filter" autocomplete="off" value="secondary" <?php echo ( $section == 'secondary' ) ? 'checked' : ''; ?> /><?php _e('Secondary'); ?>
					</label>
					<label class="btn btn-default <?php echo ( $section == 'basic literacy' ||  $section == 'all' ) ? ' active' : ''; ?>">
						<input type="radio" name="filter" autocomplete="off" value="basic literacy" <?php echo ( $section == 'basic literacy' ||  $section == 'all' ) ? 'checked' : ''; ?> /> <?php _e('Basic Literacy'); ?>
					</label>
				</div>
			</form>
			<?php endif; ?>
			<h3 class="match-resource"><?php echo count($posts) . ' Matched Resource'; ?></h3>
		</div>
		<?php if( $type == 'K to 12' ) : ?>
		<div class="float-right pb-4">
			<form method="post" class="resource-form-filter" id="resource-form">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-default <?php echo ( $section == 'all' ) ? ' active' : ''; ?>">
						<input type="radio" name="filter" autocomplete="off" value="all" <?php echo ( $section == 'all' ) ? 'checked' : ''; ?> /><?php _e('All'); ?>
					</label>
					<label class="btn btn-default <?php echo ( $section == 'sped' ) ? ' active' : ''; ?>">
						<input type="radio" name="filter" autocomplete="off" value="sped" <?php echo ( $section == 'sped' ) ? 'checked' : ''; ?> /><?php _e('SPED'); ?>
					</label>
					<label class="btn btn-default <?php echo ( $section == 'madrasah' ) ? ' active' : ''; ?>">
						<input type="radio" name="filter" autocomplete="off" value="madrasah" <?php echo ( $section == 'madrasah' ) ? 'checked' : ''; ?> /><?php _e('Madrasah'); ?>
					</label>
					<label class="btn btn-default <?php echo ( $section == 'ip education' ) ? ' active' : ''; ?>">
						<input type="radio" name="filter" autocomplete="off" value="ip education" <?php echo ( $section == 'ip education' ) ? 'checked' : ''; ?> /> <?php _e('IP Education'); ?>
					</label>
				</div>
			</form>
		</div>
		<?php endif; ?>
	</div>
	<div class="resource-list">
		<table class="table <?php echo str_replace( ' ', '-', strtolower( $type ) ); ?>" id="shortcode-table">
			<thead>
				<tr>
					<th scope="col"><?php _e('ID'); ?></th>
					<th scope="col"><?php _e('Title'); ?></th>
					<th scope="col"><?php _e('Language'); ?></th>
					<th scope="col"><?php _e('Record Type'); ?></th>
					<th scope="col"><?php _e('Action'); ?></th>
				</tr>
			</thead>	

			<tbody>
				<?php foreach( $posts as $post ) : ?>
				<tr>
					<td><?php echo $post->ID ;?></td>
					<td><?php echo $post->post_title ?></td>
					<td><?php echo get_post_meta( get_the_ID(), 'lrmds-language-type', true ) ?></td>
					<td><?php echo get_post_meta( get_the_ID(), 'lrmds-resource-type', true ) ?></td>
					<td><a href="<?php echo get_permalink( $post->ID ); ?>" class="btn btn-success btn-xs view-details">View Details</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<div class="media-list mt-4">
		<div class="clearfix my-5">
			<div class="float-left">
				<div class="media-pagination"><?php echo $pagination; ?></div>
			</div>
			<div class="float-right d-flex media-total-count">
				<span><?php _e('Total Items:'); ?></span>
				<span><?php echo $count; ?></span>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		jQuery('input[name="filter"]').on('change', function() {
			jQuery('#resource-form').submit();
		})
	});
</script>