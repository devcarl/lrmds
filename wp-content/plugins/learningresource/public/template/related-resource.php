<?php if ( ! empty( $posts ) ) : ?>
<div id="sidebar" class="related-resource py-4 px-0">
	<h4><?php _e('Related Resources'); ?></h4>
	<ul class="list-group list-group-flush">
		<?php foreach( $posts as $post ) : ?>
		<li>
			<a href="<?php echo get_permalink( $post->ID ); ?>"><?php echo $post->post_title; ?></a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>