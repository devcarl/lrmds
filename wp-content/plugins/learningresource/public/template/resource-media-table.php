<div class="resource-table media-container">

	<header class="media-header mb-4">
		<h1><?php echo ( ! empty( $type ) ) ? ucfirst( $type ) : $term->name; ?></h1>
		<hr>
		<p><?php echo ( ! empty( $type ) ) ? ucfirst( $type ) .' type' : $term->description; ?></p>
	</header>

	<div class="resource-filter  media-filter">
		<?php if( empty( $type ) ) : ?>
		<form id="form-filter" class="resource-form-filter" method="post">
			<div class="btn-group btn-group-toggle" data-toggle="buttons">
				<label class="btn btn-default <?php echo ( 'photo' == $filter ) ? 'active' : ''; ?>">
					<input type="radio" name="filter" autocomplete="off" value="photo" <?php echo ( 'photo' == $filter ) ? 'checked' : ''; ?>> <?php _e('Photo'); ?>
				</label>
				<label class="btn btn-default <?php echo ( 'illustration' == $filter ) ? 'active' : ''; ?>">
					<input type="radio" name="filter" autocomplete="off" value="illustration" <?php echo ( 'illustration' == $filter ) ? 'checked' : ''; ?>> <?php _e('Illustration'); ?>
				</label>
				<label class="btn btn-default <?php echo ( 'audio' == $filter ) ? 'active' : ''; ?>">
					<input type="radio" name="filter" autocomplete="off" value="audio" <?php echo ( 'audio' == $filter ) ? 'checked' : ''; ?>> <?php _e('Audio'); ?>
				</label>
				<label class="btn btn-default <?php echo ( 'video' == $filter ) ? 'active' : ''; ?>">
					<input type="radio" name="filter" autocomplete="off" value="video" <?php echo ( 'video' == $filter ) ? 'checked' : ''; ?>> <?php _e('Video'); ?>
				</label>
				<label class="btn btn-default <?php echo ( 'all' == $filter ) ? 'active' : ''; ?>">
					<input type="radio" name="filter" autocomplete="off" value="all" <?php echo ( 'all' == $filter ) ? 'checked' : ''; ?>> <?php _e('All'); ?>
				</label>
			</div>
		</form>
		<?php endif; ?>
	</div>

	<div class="media-list mt-4">
		<div class="container-fluid">
			<div class="row">
			<?php while ( $posts->have_posts() ) :  $posts->the_post(); ?>
				<a href="<?php echo get_permalink( get_the_ID() ); ?>" class="col-2 m-2 p-0">
					<?php the_post_thumbnail( 'thumbnail' ); ?>
				</a>
			<?php endwhile; ?>
			</div>
		</div>
	</div>
	<div class="clearfix my-5">
		<div class="float-left">
			<div class="media-pagination"><?php echo $pagination; ?></div>
		</div>
		<div class="float-right d-flex media-total-count">
			<span><?php _e('Total Items:'); ?></span>
			<span><?php echo $count; ?></span>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		jQuery('.btn-group').find('input[name="filter"]').on( 'change', function() {
			jQuery('#form-filter').submit();
		} );
	} );
</script>